import { defineNuxtConfig } from 'nuxt/config'

export default {
  devtools: { enabled: true },
  server: {
    port: 3000,
    host: 'http://localhost:6060', // Specify the host here
  },
  proxy: {
    '/api': {
      target: 'http://localhost:6060',
      changeOrigin: true,
      pathRewrite: {
        '^/api': ''
      },
    },
  },
}
