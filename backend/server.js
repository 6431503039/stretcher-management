// server.js

import express from "express";
import cors from "cors";
import routesIndex from "./routes/routesIndex.js";
import userRoutes from "./routes/userRoutes.js";
const server = express();

server.use(cors());
server.use(express.json());
server.use(express.urlencoded({ extended: true }));

// Import and use the router from routesIndex.js
server.use("/api", routesIndex);
server.use("/api", userRoutes);

server.get("/", (req, res) => {
  res.json({ message: "Hello World" });
});

const PORT = 6060;

server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
