import express from "express";
import userServices from "../services/userServices.js";

const router = express.Router();

// Route to get all users
router.get('/users', async (req, res) => {
  try {
      const users = await userServices.getAll();
      res.json(users);
  } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
  }
});
router.get('/test', async (req, res) => {
  try {
      
    res.json({ message: 'Server Start' });
  } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
  }
});


router.post('/users', async (req, res) => {
  try {
      const newUser = req.body;
      const result = await userServices.create(newUser);
      res.json(result);
  } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
  }
});


export default router; 
