// routesIndex.js

import express from 'express';

const router = express.Router();

router.get('/user', (req, res) => {
    // Handle GET request for /api/user
    res.send('GET request to /api/user');
});

router.post('/user', (req, res) => {
    // Handle POST request for /api/user
    res.send('POST request to /api/user');
});

export default router;
