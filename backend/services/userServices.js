import connection from "../config/database.js";

const getAll = async () => {
  try {
    const sql = "SELECT * FROM line_id";
    const result = await new Promise((resolve, reject) => {
      connection.query(sql, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
    return result;
  } catch (error) {
    throw error;
  }
};

const create = async (user) => {
  console.log('users = ',user);
  try {
    const sql = "INSERT INTO line_id SET ?";
    const result = await new Promise((resolve, reject) => {
      connection.query(sql, user, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
    return result;
  } catch (error) {
    throw error;
  }
};

export default { getAll, create };